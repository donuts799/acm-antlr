# Generated from ..\\HomeworkGenerator.g4 by ANTLR 4.8
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .HomeworkGeneratorParser import HomeworkGeneratorParser
else:
    from HomeworkGeneratorParser import HomeworkGeneratorParser

# This class defines a complete generic visitor for a parse tree produced by HomeworkGeneratorParser.

class HomeworkGeneratorVisitor(ParseTreeVisitor):

    # Visit a parse tree produced by HomeworkGeneratorParser#document.
    def visitDocument(self, ctx:HomeworkGeneratorParser.DocumentContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by HomeworkGeneratorParser#text.
    def visitText(self, ctx:HomeworkGeneratorParser.TextContext):
        return self.visitChildren(ctx)


    # Visit a parse tree produced by HomeworkGeneratorParser#code.
    def visitCode(self, ctx:HomeworkGeneratorParser.CodeContext):
        return self.visitChildren(ctx)



del HomeworkGeneratorParser