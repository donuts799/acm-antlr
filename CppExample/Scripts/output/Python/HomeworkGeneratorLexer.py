# Generated from ..\\HomeworkGenerator.g4 by ANTLR 4.8
from antlr4 import *
from io import StringIO
from typing.io import TextIO
import sys



def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\2\4")
        buf.write("*\b\1\4\2\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\3\2\6\2")
        buf.write("\17\n\2\r\2\16\2\20\3\3\3\3\3\3\5\3\26\n\3\3\4\3\4\3\4")
        buf.write("\3\4\7\4\34\n\4\f\4\16\4\37\13\4\3\4\3\4\3\4\3\5\3\5\3")
        buf.write("\5\5\5\'\n\5\3\6\3\6\2\2\7\3\3\5\2\7\4\t\2\13\2\3\2\5")
        buf.write("\4\2^^}}\3\2++\3\2\177\177\2*\2\3\3\2\2\2\2\7\3\2\2\2")
        buf.write("\3\16\3\2\2\2\5\25\3\2\2\2\7\27\3\2\2\2\t&\3\2\2\2\13")
        buf.write("(\3\2\2\2\r\17\5\5\3\2\16\r\3\2\2\2\17\20\3\2\2\2\20\16")
        buf.write("\3\2\2\2\20\21\3\2\2\2\21\4\3\2\2\2\22\26\n\2\2\2\23\24")
        buf.write("\7^\2\2\24\26\7}\2\2\25\22\3\2\2\2\25\23\3\2\2\2\26\6")
        buf.write("\3\2\2\2\27\30\7}\2\2\30\31\7*\2\2\31\35\3\2\2\2\32\34")
        buf.write("\5\t\5\2\33\32\3\2\2\2\34\37\3\2\2\2\35\33\3\2\2\2\35")
        buf.write("\36\3\2\2\2\36 \3\2\2\2\37\35\3\2\2\2 !\7+\2\2!\"\7\177")
        buf.write("\2\2\"\b\3\2\2\2#\'\n\3\2\2$%\7+\2\2%\'\5\13\6\2&#\3\2")
        buf.write("\2\2&$\3\2\2\2\'\n\3\2\2\2()\n\4\2\2)\f\3\2\2\2\7\2\20")
        buf.write("\25\35&\2")
        return buf.getvalue()


class HomeworkGeneratorLexer(Lexer):

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    TEXT = 1
    CODE = 2

    channelNames = [ u"DEFAULT_TOKEN_CHANNEL", u"HIDDEN" ]

    modeNames = [ "DEFAULT_MODE" ]

    literalNames = [ "<INVALID>",
 ]

    symbolicNames = [ "<INVALID>",
            "TEXT", "CODE" ]

    ruleNames = [ "TEXT", "TEXTELEMENT", "CODE", "CODE_BODY", "NOT_RIGHT_BRACE" ]

    grammarFileName = "HomeworkGenerator.g4"

    def __init__(self, input=None, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.8")
        self._interp = LexerATNSimulator(self, self.atn, self.decisionsToDFA, PredictionContextCache())
        self._actions = None
        self._predicates = None


