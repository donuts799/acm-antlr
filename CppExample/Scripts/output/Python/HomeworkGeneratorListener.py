# Generated from ..\\HomeworkGenerator.g4 by ANTLR 4.8
from antlr4 import *
if __name__ is not None and "." in __name__:
    from .HomeworkGeneratorParser import HomeworkGeneratorParser
else:
    from HomeworkGeneratorParser import HomeworkGeneratorParser

# This class defines a complete listener for a parse tree produced by HomeworkGeneratorParser.
class HomeworkGeneratorListener(ParseTreeListener):

    # Enter a parse tree produced by HomeworkGeneratorParser#document.
    def enterDocument(self, ctx:HomeworkGeneratorParser.DocumentContext):
        pass

    # Exit a parse tree produced by HomeworkGeneratorParser#document.
    def exitDocument(self, ctx:HomeworkGeneratorParser.DocumentContext):
        pass


    # Enter a parse tree produced by HomeworkGeneratorParser#text.
    def enterText(self, ctx:HomeworkGeneratorParser.TextContext):
        pass

    # Exit a parse tree produced by HomeworkGeneratorParser#text.
    def exitText(self, ctx:HomeworkGeneratorParser.TextContext):
        pass


    # Enter a parse tree produced by HomeworkGeneratorParser#code.
    def enterCode(self, ctx:HomeworkGeneratorParser.CodeContext):
        pass

    # Exit a parse tree produced by HomeworkGeneratorParser#code.
    def exitCode(self, ctx:HomeworkGeneratorParser.CodeContext):
        pass



del HomeworkGeneratorParser