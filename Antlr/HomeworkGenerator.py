import sys
from antlr4 import *
from Scripts.output.Python.HomeworkGeneratorLexer import HomeworkGeneratorLexer
from Scripts.output.Python.HomeworkGeneratorParser import HomeworkGeneratorParser
from Scripts.output.Python.HomeworkGeneratorVisitor import HomeworkGeneratorVisitor as HGV


class HomeworkGeneratorVisitor(HGV):
    def visitDocument(self, ctx:HomeworkGeneratorParser.DocumentContext):
        return [self.visit(child) for child in ctx.children]
    
    def visitText(self, ctx:HomeworkGeneratorParser.TextContext):
        return ctx.getText()
    
    def visitCode(self, ctx:HomeworkGeneratorParser.CodeContext):
        returnObject = {}
        exec(ctx.getText()[2:-2],{},returnObject)
        try:                
            return str(returnObject['result'])
        except:
            return "No Result Found"


def main(argv):
    input_stream = FileStream(argv[1])
    lexer = HomeworkGeneratorLexer(input_stream)
    stream = CommonTokenStream(lexer)
    parser = HomeworkGeneratorParser(stream)
    tree = parser.document()
    print(tree.toStringTree(["Document","Text","Code","Other"]))
    visitor = HomeworkGeneratorVisitor()
    result = visitor.visitDocument(tree)
    print(result)
    
    outputFile = open(argv[1] + ".hw", "w", newline='')
    
    for thing in result:
        if(thing == None):
            thing = ""
        outputFile.write(thing)
        print(thing)
        
    outputFile.close()
    
    
    return tree
 
if __name__ == '__main__':
    main(sys.argv)