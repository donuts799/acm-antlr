grammar HomeworkGenerator;


document : (text | code)* ;
text    : TEXT ;
code    : CODE ;
 


TEXT    : TEXTELEMENT+ ;

fragment
TEXTELEMENT    : ~[{\\]
        | '\\\\'
        | '\\{' ;

CODE : '{' ~[}]* '}' ;




