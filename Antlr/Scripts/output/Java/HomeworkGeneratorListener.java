// Generated from ..\\HomeworkGenerator.g4 by ANTLR 4.8
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link HomeworkGeneratorParser}.
 */
public interface HomeworkGeneratorListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link HomeworkGeneratorParser#document}.
	 * @param ctx the parse tree
	 */
	void enterDocument(HomeworkGeneratorParser.DocumentContext ctx);
	/**
	 * Exit a parse tree produced by {@link HomeworkGeneratorParser#document}.
	 * @param ctx the parse tree
	 */
	void exitDocument(HomeworkGeneratorParser.DocumentContext ctx);
	/**
	 * Enter a parse tree produced by {@link HomeworkGeneratorParser#text}.
	 * @param ctx the parse tree
	 */
	void enterText(HomeworkGeneratorParser.TextContext ctx);
	/**
	 * Exit a parse tree produced by {@link HomeworkGeneratorParser#text}.
	 * @param ctx the parse tree
	 */
	void exitText(HomeworkGeneratorParser.TextContext ctx);
	/**
	 * Enter a parse tree produced by {@link HomeworkGeneratorParser#code}.
	 * @param ctx the parse tree
	 */
	void enterCode(HomeworkGeneratorParser.CodeContext ctx);
	/**
	 * Exit a parse tree produced by {@link HomeworkGeneratorParser#code}.
	 * @param ctx the parse tree
	 */
	void exitCode(HomeworkGeneratorParser.CodeContext ctx);
}