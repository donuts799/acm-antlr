# Generated from ..\\HomeworkGenerator.g4 by ANTLR 4.8
# encoding: utf-8
from antlr4 import *
from io import StringIO
import sys
if sys.version_info[1] > 5:
	from typing import TextIO
else:
	from typing.io import TextIO


def serializedATN():
    with StringIO() as buf:
        buf.write("\3\u608b\ua72a\u8133\ub9ed\u417c\u3be7\u7786\u5964\3\4")
        buf.write("\26\4\2\t\2\4\3\t\3\4\4\t\4\3\2\3\2\7\2\13\n\2\f\2\16")
        buf.write("\2\16\13\2\3\2\3\2\3\3\3\3\3\4\3\4\3\4\2\2\5\2\4\6\2\2")
        buf.write("\2\24\2\f\3\2\2\2\4\21\3\2\2\2\6\23\3\2\2\2\b\13\5\4\3")
        buf.write("\2\t\13\5\6\4\2\n\b\3\2\2\2\n\t\3\2\2\2\13\16\3\2\2\2")
        buf.write("\f\n\3\2\2\2\f\r\3\2\2\2\r\17\3\2\2\2\16\f\3\2\2\2\17")
        buf.write("\20\7\2\2\3\20\3\3\2\2\2\21\22\7\3\2\2\22\5\3\2\2\2\23")
        buf.write("\24\7\4\2\2\24\7\3\2\2\2\4\n\f")
        return buf.getvalue()


class HomeworkGeneratorParser ( Parser ):

    grammarFileName = "HomeworkGenerator.g4"

    atn = ATNDeserializer().deserialize(serializedATN())

    decisionsToDFA = [ DFA(ds, i) for i, ds in enumerate(atn.decisionToState) ]

    sharedContextCache = PredictionContextCache()

    literalNames = [  ]

    symbolicNames = [ "<INVALID>", "TEXT", "CODE" ]

    RULE_document = 0
    RULE_text = 1
    RULE_code = 2

    ruleNames =  [ "document", "text", "code" ]

    EOF = Token.EOF
    TEXT=1
    CODE=2

    def __init__(self, input:TokenStream, output:TextIO = sys.stdout):
        super().__init__(input, output)
        self.checkVersion("4.8")
        self._interp = ParserATNSimulator(self, self.atn, self.decisionsToDFA, self.sharedContextCache)
        self._predicates = None




    class DocumentContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def EOF(self):
            return self.getToken(HomeworkGeneratorParser.EOF, 0)

        def text(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(HomeworkGeneratorParser.TextContext)
            else:
                return self.getTypedRuleContext(HomeworkGeneratorParser.TextContext,i)


        def code(self, i:int=None):
            if i is None:
                return self.getTypedRuleContexts(HomeworkGeneratorParser.CodeContext)
            else:
                return self.getTypedRuleContext(HomeworkGeneratorParser.CodeContext,i)


        def getRuleIndex(self):
            return HomeworkGeneratorParser.RULE_document

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterDocument" ):
                listener.enterDocument(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitDocument" ):
                listener.exitDocument(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitDocument" ):
                return visitor.visitDocument(self)
            else:
                return visitor.visitChildren(self)




    def document(self):

        localctx = HomeworkGeneratorParser.DocumentContext(self, self._ctx, self.state)
        self.enterRule(localctx, 0, self.RULE_document)
        self._la = 0 # Token type
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 10
            self._errHandler.sync(self)
            _la = self._input.LA(1)
            while _la==HomeworkGeneratorParser.TEXT or _la==HomeworkGeneratorParser.CODE:
                self.state = 8
                self._errHandler.sync(self)
                token = self._input.LA(1)
                if token in [HomeworkGeneratorParser.TEXT]:
                    self.state = 6
                    self.text()
                    pass
                elif token in [HomeworkGeneratorParser.CODE]:
                    self.state = 7
                    self.code()
                    pass
                else:
                    raise NoViableAltException(self)

                self.state = 12
                self._errHandler.sync(self)
                _la = self._input.LA(1)

            self.state = 13
            self.match(HomeworkGeneratorParser.EOF)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class TextContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def TEXT(self):
            return self.getToken(HomeworkGeneratorParser.TEXT, 0)

        def getRuleIndex(self):
            return HomeworkGeneratorParser.RULE_text

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterText" ):
                listener.enterText(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitText" ):
                listener.exitText(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitText" ):
                return visitor.visitText(self)
            else:
                return visitor.visitChildren(self)




    def text(self):

        localctx = HomeworkGeneratorParser.TextContext(self, self._ctx, self.state)
        self.enterRule(localctx, 2, self.RULE_text)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 15
            self.match(HomeworkGeneratorParser.TEXT)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx


    class CodeContext(ParserRuleContext):

        def __init__(self, parser, parent:ParserRuleContext=None, invokingState:int=-1):
            super().__init__(parent, invokingState)
            self.parser = parser

        def CODE(self):
            return self.getToken(HomeworkGeneratorParser.CODE, 0)

        def getRuleIndex(self):
            return HomeworkGeneratorParser.RULE_code

        def enterRule(self, listener:ParseTreeListener):
            if hasattr( listener, "enterCode" ):
                listener.enterCode(self)

        def exitRule(self, listener:ParseTreeListener):
            if hasattr( listener, "exitCode" ):
                listener.exitCode(self)

        def accept(self, visitor:ParseTreeVisitor):
            if hasattr( visitor, "visitCode" ):
                return visitor.visitCode(self)
            else:
                return visitor.visitChildren(self)




    def code(self):

        localctx = HomeworkGeneratorParser.CodeContext(self, self._ctx, self.state)
        self.enterRule(localctx, 4, self.RULE_code)
        try:
            self.enterOuterAlt(localctx, 1)
            self.state = 17
            self.match(HomeworkGeneratorParser.CODE)
        except RecognitionException as re:
            localctx.exception = re
            self._errHandler.reportError(self, re)
            self._errHandler.recover(self, re)
        finally:
            self.exitRule()
        return localctx





