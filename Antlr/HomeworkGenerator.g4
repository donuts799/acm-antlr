grammar HomeworkGenerator;


document : (text | code)* EOF;
text    : TEXT ;
code    : CODE ;
 


TEXT    : TEXTELEMENT+ ;

fragment TEXTELEMENT 
        : ~[{\\]
        | '\\{' ;

CODE : '{(' CODE_BODY* ')}' ;

                          
                    
                    
fragment CODE_BODY : ~[)]
        | ')' NOT_RIGHT_BRACE ;
        
fragment NOT_RIGHT_BRACE : ~[}] ;